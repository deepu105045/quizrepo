package com.quiz.daily;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by deepuvelappannair on 12/24/16.
 */

@SpringBootApplication
public class QuestionbankApplication {
    public static void main(String args[]){
        SpringApplication.run(QuestionbankApplication.class,args);
    }
}
