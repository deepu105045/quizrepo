package com.quiz.daily.questionservice.service;

import com.quiz.daily.questionservice.model.Answer;
import com.quiz.daily.questionservice.repository.AnswerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by deepuvelappannair on 12/24/16.
 */

@Service
public class AnswerService {

    @Autowired
    private AnswerRepository answerRepository;

    public String ping(){
        return "Ping received at " + LocalDateTime.now();
    }

    public List<Answer> getAnswer(Integer questionId){
        List<Answer> answerList= new ArrayList<>();
        answerRepository.findByQuestionId(questionId).forEach(answerList::add);
        return answerList;
    }

}
