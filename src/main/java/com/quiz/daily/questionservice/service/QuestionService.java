package com.quiz.daily.questionservice.service;

import com.quiz.daily.questionservice.model.Question;
import com.quiz.daily.questionservice.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by deepuvelappannair on 12/24/16.
 */

@Service
public class QuestionService {

    @Autowired
    private QuestionRepository questionRepository;

    public String ping(){
        return "Ping received at " + LocalDateTime.now();
    }

    public List<Question> getAllQuestions(){
        List<Question> questions= new ArrayList<>();
        questionRepository.findAll().forEach(questions::add);
        return questions;
    }

    public Question getQuestion(int id) {
        return questionRepository.findOne(id);
    }

    public List<Question> getQuestionContaining(String inputText) {
        List<Question> searchResults= new ArrayList<>();
        questionRepository.findByQuestionTextContaining(inputText).forEach(searchResults::add);
        return searchResults;
    }

    public Question getNextQuestion(){
        return questionRepository.findByActiveTrue().get(0);
    }

    public Question addQuestion(Question question) {
        return questionRepository.save(question);
    }
}
