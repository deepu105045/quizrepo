package com.quiz.daily.questionservice.controller;

import com.quiz.daily.questionservice.model.Answer;
import com.quiz.daily.questionservice.model.Question;
import com.quiz.daily.questionservice.service.AnswerService;
import com.quiz.daily.questionservice.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by deepuvelappannair on 12/24/16.
 */

@RestController
@RequestMapping("/api")
public class QuestionController {

    @Autowired
    private QuestionService questionService;
    @Autowired
    private AnswerService answerService;

    @RequestMapping("/ping")
    public ResponseEntity<String> ping(){
        return new ResponseEntity<String>(questionService.ping(),HttpStatus.OK);
    }

    @RequestMapping("/questions")
    public ResponseEntity<List<Question>> getAllQuestions(){
        return new ResponseEntity<List<Question>>(questionService.getAllQuestions(), HttpStatus.OK);
    }
    @RequestMapping("/questions/{id}")
    public ResponseEntity<Question> getQuestion(@PathVariable int id){
        return new ResponseEntity<Question>(questionService.getQuestion(id),HttpStatus.OK);
    }
    @RequestMapping("/questions/containing/{inputText}")
    public ResponseEntity<List<Question>> getQuestionContaining(@PathVariable  String inputText){
        return new ResponseEntity<List<Question>>(questionService.getQuestionContaining(inputText),HttpStatus.OK);
    }

    @RequestMapping("/question")
    public ResponseEntity<Question> getNextQuestion(){
        return new ResponseEntity<Question>(questionService.getNextQuestion(),HttpStatus.OK);
    }

    @RequestMapping("questions/{id}/answer")
    public ResponseEntity<List<Answer>> getAnswer(@PathVariable int id){
        return new ResponseEntity<List<Answer>>(answerService.getAnswer(id),HttpStatus.OK);
    }

    @RequestMapping(value = "/questions",
                    method = RequestMethod.POST,
                    produces = MediaType.APPLICATION_JSON_VALUE,
                    consumes =MediaType.APPLICATION_JSON_VALUE
                    )
    public ResponseEntity<Question> addQuestion(@RequestBody Question question){
        return new ResponseEntity<Question>(questionService.addQuestion(question),HttpStatus.CREATED);
    }


}
