package com.quiz.daily.questionservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@IdClass(OptionPK.class)
@Entity
@Table(name = "options")
@Data
public class Option implements Serializable{
    @Id
    @JsonIgnore
    private int questionId;
    @Id
    private String optionsId;
    @Id
    private String optionText;


}
