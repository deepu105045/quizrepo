package com.quiz.daily.questionservice.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by deepuvelappannair on 12/24/16.
 */

@Entity
@Table(name="answers")
@Data
public class Answer {
    @Id
    private int id;
    private int questionId;
    private String answerId;
}
