package com.quiz.daily.questionservice.model;

import lombok.Data;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by deepuvelappannair on 12/26/16.
 */

@Embeddable
@Data
public class OptionPK implements Serializable{
    private int questionId;
    private String optionsId;
    private String optionText;
}
