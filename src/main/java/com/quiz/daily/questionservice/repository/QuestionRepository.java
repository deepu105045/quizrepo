package com.quiz.daily.questionservice.repository;

import com.quiz.daily.questionservice.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by deepuvelappannair on 12/24/16.
 */
public interface QuestionRepository extends JpaRepository<Question,Integer> {
    public List<Question> findByQuestionTextContaining(String value);
    public List<Question> findByActiveTrue();
}
