package com.quiz.daily.questionservice.repository;

import com.quiz.daily.questionservice.model.Answer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by deepuvelappannair on 12/24/16.
 */
public interface AnswerRepository extends JpaRepository<Answer, Integer> {
    public List<Answer> findByQuestionId(int questionId);
}
