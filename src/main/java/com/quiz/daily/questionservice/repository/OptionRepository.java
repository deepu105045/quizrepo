package com.quiz.daily.questionservice.repository;

import com.quiz.daily.questionservice.model.Option;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by deepuvelappannair on 12/26/16.
 */
public interface OptionRepository extends JpaRepository<Option,Integer> {
}
