CREATE TABLE questions (
  questionId INTEGER ,
  questionText VARCHAR(100) NOT NULL,
  questionType VARCHAR (10),
  active BOOLEAN,
  PRIMARY KEY(questionId)
);

CREATE TABLE options(
  questionId INTEGER,
  optionsId VARCHAR(1),
  optionText VARCHAR(100),
  PRIMARY KEY (questionId,optionsId),
   FOREIGN KEY (questionId) REFERENCES questions (questionId)
);

CREATE TABLE answers(
  id INTEGER,
  questionId INTEGER,
  answerId VARCHAR(100),
  PRIMARY KEY (id),
  FOREIGN KEY (questionId)  REFERENCES questions (questionId)
);
